<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 3/13/16
 * Time: 2:02 PM
 */
/**
 * Class login
 * handles the user's login and logout process
 */
class Login
{

    public function __construct()
    {
        // create/read session, absolutely necessary
        session_start();
        // check the possible login actions:
        // if user tried to log out (happen when user clicks logout button)
        if (isset($_GET["logout"])) {
            $this->doLogout();
        }
        // login via post data (if user just submitted a login form)
        elseif (isset($_POST["login"])) {
            $this->dologinWithPostData();
        }
    }
    /**
     * log in with post data
     */
    public function login($username, $password)
    {
        // check login form contents

        // create a database connection, using the constants from config/db.php (which we loaded in index.php)
        $db = new PDO(Config::DSN, Config::DB_USERNAME, Config::DB_PASSWORD);

        // database query, getting all the info of the selected user (allows login via email address in the
        // username field)
        $query = "SELECT * FROM users WHERE username = '$username' AND password = '$password'";
        $stmt = $db->query($query);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        // if this user exists
        if (count($result) > 0) {
            // using PHP 5.5's password_verify() function to check if the provided password fits
            // the hash of that user's password// write user data into PHP SESSION (a file on your server)
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;
            $_SESSION['logged_in'] = 1;
        }

}

    /**
     * perform the logout
     */
    public function logout()
    {
        // delete the session of the user
        $_SESSION = array();
        session_destroy();
        // return a little feeedback message
    }
    /**
     * simply return the current state of the user's login
     * @return boolean user's login status
     */
    public function isUserLoggedIn()
    {
        if (isset($_SESSION['logged_in']) AND $_SESSION['logged_in'] == 1) {
            return true;
        }
        // default return
        return false;
    }
}