<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 3/25/16
 * Time: 5:50 PM
 */
interface ITapSvc{


    const DSN = Config::DSN;
    const USER = Config::DB_USERNAME;
    const PASS = Config::DB_PASSWORD;

    public function get($id);
    public function getAll();
    public function post($object);
    public function put($object);
    public function patch($object);
    public function delete($id);

}