<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 3/25/16
 * Time: 10:15 PM
 */
class BrewerySvc implements ITapSvc{

    public function get($id){
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $query = "SELECT * FROM brewery WHERE id = $id";
        $stmt = $dbc->query($query);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $dbc = null ;
        return $result;
    }

    public function getAll(){
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $query = "SELECT * FROM brewery ORDER BY name ASC ";
        $stmt = $dbc->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $dbc = null ;
        return $result;
    }

    public function post($object){
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $statement = $dbc->prepare("INSERT INTO brewery
        (name, description, state, url) VALUES
        (:name, :description, :state, :url)");

        //bind values
        $statement->bindValue(":name", $object->getName());
        $statement->bindValue(":description", $object->getDescription());
        $statement->bindValue(":state", $object->getState());
        $statement->bindValue(":url", $object->getUrl());
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;

    }

    public function put($object)
    {
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $statement = $dbc->prepare("UPDATE brewery SET
        name = :name, description = :description, state = :state, url = :url
        WHERE id = :id");

        //bind values
        $statement->bindValue(":name", $object->getName());
        $statement->bindValue(":description", $object->getDescription());
        $statement->bindValue(":state", $object->getState());
        $statement->bindValue(":url", $object->getUrl());
        $statement->bindValue(":id", $object->getId());
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }

    public function patch($object)
    {
        // TODO: Implement patch() method.
    }

    public function delete($id)
    {
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $statement = $dbc->prepare("DELETE FROM brewery WHERE id = :id");
        $statement->bindValue(':id', $id);
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }
}