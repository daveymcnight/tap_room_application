<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 3/25/16
 * Time: 10:34 PM
 */
class TypeSvc implements ITapSvc
{
    public function get($id){
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $query = "SELECT * FROM type WHERE id = $id";
        $stmt = $dbc->query($query);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $dbc = null ;
        return $result;
    }

    public function getAll(){
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $query = "SELECT * FROM type";
        $stmt = $dbc->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $dbc = null ;
        return $result;
    }

    public function post($object)
    {
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $statement = $dbc->prepare("INSERT INTO type
        (name_short, name_long, description) VALUES
        (:name_short, :name_long, :description)");
        //bind values
        $statement->bindValue(":name_short", $object->getNameShort());
        $statement->bindValue(":name_long", $object->getNameLong());
        $statement->bindValue(":description", $object->getDescription());

        //bind values

        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }

    public function put($object)
    {
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $statement = $dbc->prepare("UPDATE type SET
        name_short = :name_short, name_long = :name_long, description = :description
        WHERE id = :id");

        //bind values
        $statement->bindValue(":name_short", $object->getNameShort());
        $statement->bindValue(":name_long", $object->getNameLong());
        $statement->bindValue(":description", $object->getDescription());
        $statement->bindValue(":id", $object->getId());
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }

    public function patch($object)
    {
        // TODO: Implement patch() method.
    }

    public function delete($id)
    {
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $statement = $dbc->prepare("DELETE FROM type WHERE id = :id");
        $statement->bindValue(':id', $id);
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }

}