<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 3/25/16
 * Time: 5:52 PM
 */


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


class BeerSvc implements ITapSvc{

    public function get($id){

        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);

        $query = "SELECT beer.id, beer.name as beer_name, beer.description, beer.abv,
                    beer.price_pint, beer.price_32_oz, beer.price_64_oz, beer.price_2liter,
                    beer.url, beer.rating,
                    brewery.name as brewery_name, beer.brewery_id, brewery.state,
                    beer.type_id, type.name_short,
                    beer.is_10_oz, beer.is_growler_available
                  FROM beer
                  INNER JOIN brewery on beer.brewery_id = brewery.id
                  INNER JOIN type on beer.type_id = type.id
                  WHERE beer.id = $id";

        $stmt = $dbc->query($query);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $dbc = null;
        return $result;
    }

    public function getAll(){

        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $query = "SELECT beer.id, beer.name as beer_name, beer.description, beer.abv,
                    beer.price_pint, beer.price_32_oz, beer.price_64_oz, beer.price_2liter,
                    beer.url, beer.rating,
                    brewery.name as brewery_name, beer.brewery_id, brewery.state,
                    beer.type_id, type.name_short as type_name_short, type.name_long as type_name_long,
                    beer.is_10_oz, beer.is_growler_available
                  FROM beer
                  INNER JOIN brewery on beer.brewery_id = brewery.id
                  INNER JOIN type on beer.type_id = type.id
                  ORDER BY beer_name ASC";
        $stmt = $dbc->query($query);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $results;
    }

    public function post($object){

        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);

        $statement = $dbc->prepare("INSERT INTO beer
            (name, description,
             type_id, brewery_id, abv, price_pint,
             price_32_oz, price_64_oz, price_2liter,
             url, rating, is_10_oz, is_growler_available) VALUES
             (:name, :description,
             :type_id, :brewery_id, :abv, :price_pint,
             :price_32_oz, :price_64_oz, :price_2liter,
             :url, :rating, :is_10_oz, :is_growler_available)");
        //bind value
        $statement->bindValue(':name', $object->getName());
        $statement->bindValue(':description', $object->getDescription());
        $statement->bindValue(':type_id', $object->getTypeId());
        $statement->bindValue(':brewery_id', $object->getBreweryId());
        $statement->bindValue(':abv', $object->getAbv());
        $statement->bindValue(':price_pint', $object->getPricePint());
        $statement->bindValue(':price_32_oz', $object->getPrice32Oz());
        $statement->bindValue(':price_64_oz', $object->getPrice64Oz());
        $statement->bindValue(':price_2liter', $object->getPrice2liter());
        $statement->bindValue(':url', $object->getUrl());
        $statement->bindValue(':rating', $object->getRating());
        $statement->bindValue(':is_10_oz', $object->getIs10Oz());
        $statement->bindValue(':is_growler_available', $object->getIsGrowlerAvailable());

        //execute
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }

    public function put($object){
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);


        $statement = $dbc->prepare("UPDATE beer
             SET name = :name, description = :description,
             type_id = :type_id, brewery_id = :brewery_id, abv = :abv, 
             price_pint = :price_pint, price_32_oz = :price_32_oz, price_64_oz = :price_64_oz,
             price_2liter = :price_2liter, url = :url, rating = :rating, is_10_oz = :is_10_oz,
             is_growler_available = :is_growler_available WHERE id = :id");
        
        
        //bind value
        $statement->bindValue(':id', $object->getId());
        $statement->bindValue(':name', $object->getName());
        $statement->bindValue(':description', $object->getDescription());
        $statement->bindValue(':type_id', $object->getTypeId());
        $statement->bindValue(':brewery_id', $object->getBreweryId());
        $statement->bindValue(':abv', $object->getAbv());
        $statement->bindValue(':price_pint', $object->getPricePint());
        $statement->bindValue(':price_32_oz', $object->getPrice32Oz());
        $statement->bindValue(':price_64_oz', $object->getPrice64Oz());
        $statement->bindValue(':price_2liter', $object->getPrice2liter());
        $statement->bindValue(':url', $object->getUrl());
        $statement->bindValue(':rating', $object->getRating());
        $statement->bindValue(':is_10_oz', $object->getIs10Oz());
        $statement->bindValue(':is_growler_available', $object->getIsGrowlerAvailable());

        //execute
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }

    public function patch($object){

    }

    public function delete($id){
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $statement = $dbc->prepare("DELETE FROM beer WHERE id = :id");
        $statement->bindValue(':id', $id);
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }

    public function getBeersOnTap(){

        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $query = "SELECT tap.id, tap.beer_id, beer.name as beer_name, beer.description,
          CONCAT('$', FORMAT(beer.price_pint, 2))as price_pint, CONCAT('$', FORMAT(beer.price_32_oz,2)) as price_32_oz,
          CONCAT('$', FORMAT(beer.price_64_oz,2)) as price_64_oz, CONCAT('$', FORMAT(beer.price_2liter,2)) as price_2liter,
          beer.url, beer.rating, beer.abv,
          beer.brewery_id, brewery.name as brewery_name, brewery.state as brewery_state,
          beer.type_id, type.name_short as beer_type_name
          FROM tap
          INNER JOIN beer on tap.beer_id = beer.id
          INNER JOIN brewery on beer.brewery_id = brewery.id
          INNER JOIN type on beer.type_id = type.id";
        $stmt = $dbc->query($query);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $dbc = null;
        return $results;
    }


}