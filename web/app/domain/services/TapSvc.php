<?php

/**
 * Created by PhpStorm.
 * User: Davey
 * Date: 4/6/16
 * Time: 7:47 PM
 */
class TapSvc implements ITapSvc{

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $query = "SELECT * FROM tap";
        $stmt = $dbc->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $dbc = null ;
        return $result;
    }

    public function post($object)
    {
      
    }

    public function put($object)
    {


        $dbc = new PDO(ITapSvc::DSN, ITapSvc::USER, ITapSvc::PASS);
        $statement = $dbc->prepare("UPDATE tap SET beer_id = :beer_id WHERE id = :id");

        //bind values
        $statement->bindValue(":id", $object->getId());
        $statement->bindValue(":beer_id", $object->getBeerId());
        $statement->execute();
        $statement->closeCursor();
        $dbc = null;
    }

    public function patch($object)
    {
        // TODO: Implement patch() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}