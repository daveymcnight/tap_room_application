<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 2/2/16
 * Time: 10:48 PM
 */

class Type{


    private $id;
    private $name_short; //abbreviation (like IPA)
    private $name_long;
    private $description;

    /**
     * Type constructor.
     * @param $name_short
     * @param $name_long
     * @param $description
     */
    public function __construct(){}

    public function setFields($params){

        if(isset($params["id"])){
            $this-> id = $params["id"];
        }
        $defaults = array(
            "name_short" => "N. Short",
            "name_long" => "N. Long",
            "description" => "No Desc",
        );

        $parameters = array_merge($defaults, $params);

        //set object values
        $this->name_long = $parameters['name_short'];
        $this->name_short = $parameters['name_long'];
        $this->description = $parameters['description'];;
    }

    /**
     * @return mixed
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNameShort(){
        return $this->name_short;
    }

    /**
     * @return mixed
     */
    public function getNameLong(){
        return $this->name_long;
    }

    /**
     * @return mixed
     */
    public function getDescription(){
        return $this->description;
    }




}