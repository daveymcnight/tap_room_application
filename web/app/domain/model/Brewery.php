<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 2/2/16
 * Time: 10:48 PM
 */
class Brewery{

    private $id;
    private $name;
    private $description;
    private $state; //2 late state postal code
    private $url;

    /**
     * Brewery constructor.
     * @param $name
     * @param $description
     * @param $state
     * @param $url
     */
    public function __construct(){

    }

    public function setFields($params){

        if(isset($params["id"])){
            $this-> id = $params["id"];
        }


        $defaults = array(
            "name" => "No Name",
            "description" => "No description",
            "url" => "",
            "rating" => "",

        );

        //mere with parameters
        $parameters = array_merge($defaults, $params);

        //set object values
        $this->name = $parameters['name'];
        $this->description = $parameters['description'];;
        $this->state = $parameters['state'];;
        $this->url = $parameters['url'];;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    } //link to website or something like that



}

