<?php

/**
 * Created by PhpStorm.
 * User: Davey
 * Date: 4/6/16
 * Time: 7:39 PM
 */
class Tap{

    private $id;
    private $beer_id;
    
    

    /**
     * Tap constructor.
     */
    public function __construct()
    {

    }

    public function setFields($params)
    {
        if(isset($params["id"])){
            $this-> id = $params["id"];
        }
        $defaults = array(
            "beer_id" => 0,
        );

        $parameters = array_merge($defaults, $params);

        //set object values
        $this->id = $parameters['id'];
        $this->beer_id = $parameters['beer_id'];
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBeerId()
    {
        return $this->beer_id;
    }

    /**
     * @param mixed $beer_id
     */
    public function setBeerId($beer_id)
    {
        $this->beer_id = $beer_id;
    }

    /**
     * @return mixed
     */
    public function getTagId()
    {
        return $this->tag_id;
    }

    /**
     * @param mixed $tag_id
     */
    public function setTagId($tag_id)
    {
        $this->tag_id = $tag_id;
    }



}