<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 2/2/16
 * Time: 10:48 PM
 */

/* This is the beer class. This class will store information like Beer Name, Description, Price
 * and some flags that will help the app display the information correctly
 */

class Beer{

    //Fields

    //basic info
    private $id; //primary key
    private $name;
    private $description; //description that will be on the web page
    private $abv;
    private $url; //url to page (i.e.) beer advocate --> use on webview of mobile app
    private $rating;
    //Foreign Keys
    private $type_id; //FK for the beer type
    private $brewery_id; //FK for the brewery

    // price info
    private $price_half_pint;
    private $price_pint;
    private $price_32_oz;
    private $price_64_oz;
    private $price_2liter;

    /**
     * @return mixed
     */


    //booleans/flags

    private $is_10_oz; //if the pint glass is actually 10oz pour
    private $is_growler_available;

    public function setFields($params){
        if($params != null) {
            //set default

            //if an ID is passed (Always will besides post)
            if(isset($params["id"])){
                $this-> id = $params["id"];
            }

            $defaults = array(
                "name" => 0,
                "tap_number" => 0,
                "description" => "No description",
                "url" => "",
                "rating" => 0,
                "price_32_oz" => 0,
                "price_64_oz" => 0,
                "price_2liter" => 0,
                "is_10_oz" => 0,
                "is_growler_available" => 0
            );
            //mere with parameters
            $parameters = array_merge($defaults, $params);

            //set object values
            $this->name = $parameters["name"];
            $this->tap_number = $parameters["tap_number"];
            $this->description = $parameters["description"];
            $this->abv = $parameters["abv"];
            $this->url = $parameters["url"];
            $this->rating = $parameters["rating"];
            $this->type_id = $parameters["type_id"];
            $this->brewery_id = $parameters["brewery_id"];
            $this->price_pint = $parameters["price_pint"];
            $this->price_32_oz = $parameters["price_32_oz"];
            $this->price_64_oz = $parameters["price_64_oz"];
            $this->price_2liter = $parameters["price_2liter"];
            $this->is_10_oz = $parameters["is_10_oz"];
            $this->is_growler_available = $parameters["is_growler_available"];
            
        }
    }

    public function __construct(){

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getAbv()
    {
        return $this->abv;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * @return mixed
     */
    public function getBreweryId()
    {
        return $this->brewery_id;
    }

    /**
     * @return mixed
     */
    public function getPriceHalfPint()
    {
        return $this->price_half_pint;
    }

    /**
     * @return mixed
     */
    public function getPricePint()
    {
        return $this->price_pint;
    }

    /**
     * @return mixed
     */
    public function getPrice32Oz()
    {
        return $this->price_32_oz;
    }

    /**
     * @return mixed
     */
    public function getPrice64Oz()
    {
        return $this->price_64_oz;
    }

    /**
     * @return mixed
     */
    public function getPrice2liter()
    {
        return $this->price_2liter;
    }

    /**
     * @return mixed
     */
    public function getIs10Oz()
    {
        return $this->is_10_oz;
    }

    /**
     * @return mixed
     */
    public function getIsGrowlerAvailable()
    {
        return $this->is_growler_available;
    }




}