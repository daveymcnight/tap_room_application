<?php

/**
 * Created by PhpStorm.
 * User: Davey
 * Date: 4/22/16
 * Time: 5:50 PM
 */
class Event{

    private $id;
    private $name;
    private $who;
    private $type;
    private $description;
    private $cost;

    /**
     * Event constructor.
     */
    public function __construct(){

    }


    public function setFields($params){
        if(isset($params["id"])){
            $this-> id = $params["id"];
        }
        $defaults = array(
            "beer_id" => 0,
        );

        $parameters = array_merge($defaults, $params);

        //set object values
        $this->id = $parameters['id'];
        $this->name = $parameters['name'];
        $this->who = $parameters['who'];
        $this->type = $parameters['type'];
        $this->description = $parameters['description'];
        $this->cost = $parameters['cost'];
    }

    /**
     * @return mixed
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getWho(){
        return $this->who;
    }

    /**
     * @return mixed
     */
    public function getType(){
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCost(){
        return $this->cost;
    }




}