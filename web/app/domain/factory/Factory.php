<?php

/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 3/25/16
 * Time: 6:01 PM
 */
class Factory {

    private static $instance;

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct(){

    }

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function getInstance(){
        if (null === static::$instance) {
            static::$instance = new Factory();
        }
        return static::$instance;
    }

    //this returns the models
    public function getModel($className){
        if($className){
            return new $className;
        }
        return null;

    }

    public function getService($obj){
        if($obj){
            $domainClass = get_class($obj);
            $serviceClass = $domainClass . "Svc";
            return new $serviceClass;
        }
        return null;
    }

}