# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: taproom
# Generation Time: 2016-03-30 22:39:05 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table beer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `beer`;

CREATE TABLE `beer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `tap_number` int(2) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `brewery_id` int(11) NOT NULL,
  `abv` varchar(10) NOT NULL DEFAULT '',
  `price_half_pint` float DEFAULT NULL,
  `price_pint` float NOT NULL,
  `price_32_oz` float DEFAULT NULL,
  `price_64_oz` float DEFAULT NULL,
  `price_2liter` float DEFAULT NULL,
  `is_10_oz` tinyint(1) NOT NULL DEFAULT '0',
  `is_growler_available` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(100) DEFAULT NULL,
  `rating` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `beer` WRITE;
/*!40000 ALTER TABLE `beer` DISABLE KEYS */;

INSERT INTO `beer` (`id`, `name`, `tap_number`, `description`, `type_id`, `brewery_id`, `abv`, `price_half_pint`, `price_pint`, `price_32_oz`, `price_64_oz`, `price_2liter`, `is_10_oz`, `is_growler_available`, `url`, `rating`)
VALUES
	(1,'Sculpin',1,'Creates hints of apricot, peach, mango and lemon flavors, but still packs a bit of a sting, just like a Sculpin fish.',1,1,'7.2',NULL,6.5,11,16,20,0,1,'http://www.beeradvocate.com/beer/profile/199/29619/',97),
	(2,'Duet',4,'Really really good beer',1,2,'7.9',NULL,7,12,17,21,0,1,NULL,NULL),
	(4,'Hoppy Birthday',0,'Not as good as the duet.',1,2,'6.2',NULL,5,9,13,16,0,1,'http://',85),
	(5,'Hop Hunter',0,'Hoppy Beer by SN',1,3,'6.9',NULL,12,12,12,12,1,1,'12',12),
	(12,'Nooner',0,'Good stand pilsner.',4,3,'5.1',NULL,4,7,12,14,0,1,'http://www.beeradvocate.com/beer/profile/140/144200/',87),
	(14,'Torpedo Extra IPA',0,'Kinda of a good beer.',1,3,'7.6',NULL,5,9,16,18,0,0,'http://www.beeradvocate.com/beer/profile/140/144200/',87);

/*!40000 ALTER TABLE `beer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table brewery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `brewery`;

CREATE TABLE `brewery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `state` varchar(2) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `brewery` WRITE;
/*!40000 ALTER TABLE `brewery` DISABLE KEYS */;

INSERT INTO `brewery` (`id`, `name`, `state`, `country`, `url`, `description`)
VALUES
	(1,'Ballast Point','CA',NULL,NULL,NULL),
	(2,'Alpine','CA',NULL,NULL,NULL),
	(3,'Sierra Nevada','NC',NULL,'http://www.sierranevada.com/','Awesome Brewery in NC');

/*!40000 ALTER TABLE `brewery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tap
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tap`;

CREATE TABLE `tap` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `beer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tap` WRITE;
/*!40000 ALTER TABLE `tap` DISABLE KEYS */;

INSERT INTO `tap` (`id`, `beer_id`)
VALUES
	(1,NULL),
	(2,NULL),
	(3,NULL),
	(4,NULL),
	(5,NULL),
	(6,NULL),
	(7,NULL),
	(8,NULL),
	(9,NULL),
	(10,NULL),
	(11,NULL),
	(12,NULL),
	(13,NULL),
	(14,NULL),
	(15,NULL),
	(16,NULL),
	(17,NULL),
	(18,NULL),
	(19,NULL),
	(20,NULL);

/*!40000 ALTER TABLE `tap` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `type`;

CREATE TABLE `type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name_short` varchar(10) NOT NULL DEFAULT '',
  `name_long` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;

INSERT INTO `type` (`id`, `name_short`, `name_long`, `description`)
VALUES
	(1,'IPA','India Pale Ale','Hoppy bitter beer.'),
	(2,'Porter','Porter','Malty dark beer.'),
	(3,'ESB','English Standard Ale','English Standard Ale'),
	(4,'Pil','Pilsner','Standard Beer');

/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL DEFAULT '',
  `password` varchar(25) NOT NULL DEFAULT '',
  `role` varchar(10) NOT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `role`, `first_name`, `last_name`, `email`, `phone`)
VALUES
	(1,'davey','davey','admin','Davey','McNight','davidmcnight@gmail.com','864-313-7438');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
