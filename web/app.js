/**
 * Created by McDavis on 2/2/16.
 */


var tapRoom = angular.module('tapRoom',['angularUtils.directives.dirPagination','ngAnimate']);

tapRoom.controller('beerController', ['$scope','$http', function($scope, $http) {
    
    $scope.controllerName = "beerController";
    
    //Need initialize ID -- it is not in both forms
    $scope.id = "";

    //Pagination
    $scope.currentPage = 1;
    $scope.pageSize = 10;

    //Booleans to hide/show elements
    $scope.showForm = false;
    $scope.showAddButton = false;
    $scope.showUpdateButton = false;

    //Booleans to hide/show error messages
    $scope.saveSuccess = false;
    $scope.saveFail = false;
    $scope.deleteSuccess = false;
    $scope.deleteFail = false;

    $scope.getBeersOnTap = function () {
        $http.get("/api/Beer/onTap").success(function (response) {
            $scope.beersOnTap = response;
        });
    }


    $scope.getBeers = function () {
        $http.get("/api/Beer").success(function (response) {
            $scope.beers = response;
        });
    }

    $scope.getBeer = function (id) {
        $http.get("/api/Beer/" + id).success(function (response) {
            $scope.beer = response;
        });
    }

    $scope.addBeer = function () {
        $http.post("/api/Beer", JSON.stringify({
            'name': $scope.beer_name, 'description': $scope.description,
            'brewery_id': $scope.brewery_id, 'type_id': $scope.type_id,
            'abv': $scope.abv, 'price_pint': $scope.price_pint,
            'price_32_oz': $scope.price_32_oz, 'price_64_oz': $scope.price_64_oz,
            'price_2liter': $scope.price_2liter, 'url': $scope.url, 'rating': $scope.rating,
            'is_growler_available': $scope.is_growler_available, 'is_10_oz': $scope.is_10_oz
        })).then(function successCallback(response) {
            $scope.saveSuccess = true;
            $scope.showForm = false;
            $scope.showAddButton = false;
            $scope.getBeers();
        }, function errorCallback(response) {
            $scope.saveFail = true;
        });
    }


    $scope.updateBeer = function () {
        $http.put("/api/Beer", JSON.stringify({
            'id': $scope.id,
            'name': $scope.beer_name, 'description': $scope.description,
            'brewery_id': $scope.brewery_id, 'type_id': $scope.type_id,
            'abv': $scope.abv, 'price_pint': $scope.price_pint,
            'price_32_oz': $scope.price_32_oz, 'price_64_oz': $scope.price_64_oz,
            'price_2liter': $scope.price_2liter, 'url': $scope.url, 'rating': $scope.rating,
            'is_growler_available': $scope.is_growler_available, 'is_10_oz': $scope.is_10_oz
        })).then(function successCallback(response) {
            $scope.getBeers();
            $scope.saveSuccess = true;
            $scope.showForm = false;

        }, function errorCallback(response) {
            $scope.saveFail = true;
        });
    }

    $scope.deleteBeer = function (id) {
        $http.delete("/api/Beer/" + id).then(function successCallback(response) {
            $scope.deleteSuccess = true;
            $scope.getBeers();
            $scope.clearBeerForm();
            $scope.showAddButton = true;
            $scope.showUpdateButton = false;
        }, function errorCallback(response) {
            $scope.deleteFail = true;
        });
    }

    $scope.clearBeerForm = function () {
        $scope.id = "";
        $scope.beer_name = "";
        $scope.description = "";
        $scope.brewery_id = "";
        $scope.type_id = "";
        $scope.abv = "";
        $scope.price_pint = "";
        $scope.price_32_oz = "";
        $scope.price_64_oz = "";
        $scope.price_2liter = "";
        $scope.url = "";
        $scope.rating = "";
        $scope.is_10_oz = "";
        $scope.is_growler_available = "";
        $scope.showAddButton = true;
        $scope.showUpdateButton = false;
    }

    $scope.fillBeerDetail = function ($index) {


        $scope.d_beer_name = $scope.beers[$index].beer_name;
        $scope.d_beer_description = $scope.beers[$index].description;
        $scope.d_brewery_id = $scope.beers[$index].brewery_id;
        $scope.d_brewery_name = $scope.beers[$index].brewery_name;
        $scope.d_type_name = $scope.beers[$index].type_name_short;
        $scope.d_price_pint =  $scope.price_pint = $scope.beers[$index].price_pint;
        var val = $index + 1;
        var element = jQuery( "#list-master div:nth-child(" + val + ")");
        console.log(element);
        changeMasterListStyles(element);
    }


    $scope.fillBeerForm = function ($index) {

        $scope.showAddButton = false;
        $scope.showForm = true;

        //basic
        $scope.id = $scope.beers[$index].id;
        $scope.beer_name = $scope.beers[$index].beer_name;
        $scope.description = $scope.beers[$index].description;
        $scope.brewery_id = $scope.beers[$index].brewery_id;
        $scope.type_id = $scope.beers[$index].type_id;
        $scope.abv = $scope.beers[$index].abv;

        //price
        $scope.price_pint = $scope.beers[$index].price_pint;
        $scope.price_32_oz = $scope.beers[$index].price_32_oz;
        $scope.price_64_oz = $scope.beers[$index].price_64_oz;
        $scope.price_2liter = $scope.beers[$index].price_2liter;

        //BA info
        $scope.url = $scope.beers[$index].url;
        $scope.rating = $scope.beers[$index].rating;

        //booleans
        $scope.is_10_oz = $scope.beers[$index].is_10_oz;
        $scope.is_growler_available = $scope.beers[$index].is_growler_available;
        $scope.showUpdateButton = true;

    }

    $scope.setupAddBeer = function () {
        $scope.showUpdateButton = false;
        $scope.clearBeerForm();
    }
    $scope.getTypes = function () {
        $http.get("/api/Type").success(function (response) {
            $scope.types = response;
        });
    }

    $scope.getBreweries = function () {
        $http.get("/api/Brewery").success(function (response) {
            $scope.breweries = response;
        });
    }

}]);


tapRoom.controller('typesController', ['$scope','$http', function($scope, $http) {

    $scope.getTypes = function () {
        $http.get("/api/Type").success(function (response) {
            $scope.types = response;
        });
    }

}]);

tapRoom.controller('breweryController', ['$scope','$http', function($scope, $http) {

    $scope.getBreweries = function () {
        $http.get("/api/Brewery").success(function (response) {
            $scope.Brewery = response;
        });
    }

}]);

tapRoom.controller('tapController', ['$scope','$http', function($scope, $http) {
    
    $scope.getTaps = function () {
        $http.get("/api/Tap").success(function (response) {
            $scope.taps = response;
        });
    }
    
    $scope.updateTap = function ($index) {
        console.log($scope.taps[$index].id);
        console.log($scope.taps[$index].beer_id);

        var id = $scope.taps[$index].id;
        var beer_id = $scope.taps[$index].beer_id;
        $http.put("/api/Tap", JSON.stringify({
            'id': id, 'beer_id': beer_id,
        })).then(function successCallback(response) {
            console.log("Success");
            $scope.saveSuccess = true;
        }, function errorCallback(response) {
            console.log("Fail");
        });
    }

    $scope.getBeers = function () {
        $http.get("/api/Beer").success(function (response) {
            $scope.beers = response;
        });
    }

}]);





function OtherController($scope) {
    $scope.pageChangeHandler = function(num) {
        console.log('going to page ' + num);
    };
}

tapRoom.controller('OtherController', OtherController);



var module = angular.module( "tapRoom" );
module.directive( "mwConfirmClick", [
    function( ) {
        return {
            priority: -1,
            restrict: 'A',
            scope: { confirmFunction: "&mwConfirmClick" },
            link: function( scope, element, attrs ){
                element.bind( 'click', function( e ){
                    // message defaults to "Are you sure?"
                    var message = attrs.mwConfirmClickMessage ? attrs.mwConfirmClickMessage : "Are you sure?";
                    // confirm() requires jQuery
                    if( confirm( message ) ) {
                        scope.confirmFunction();
                    }
                });
            }
        }
    }
]);