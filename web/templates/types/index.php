<?php
/**
 * Created by PhpStorm.
 * User: McDavis
 * Date: 2/24/16
 * Time: 10:11 PM
 */
require_once "templates/header.php";
?>
<div class="container theme-showcase" role="main" ng-app="tapRoom" ng-controller="beerController" ng-init="getTypes();">
    <div class="jumbotron row mar-bot-15">
        <h1>Types <p>These are the types of beer currently in the system.</p></h1>
    </div>
    <div class="row">
        <div ng-show="saveSuccess" class="alert alert-success " role="alert">
            <p>The type has been saved to the database.</p>
        </div>
    </div>

    <div class="row">
        <div ng-show="saveFail" class="alert alert-danger" role="alert">
            <p>There was an error saving the type to the database.</p>
        </div>
    </div>

    <div class="row">
        <div ng-show="deleteSuccess" class="alert alert-success" role="alert">
            <p>The type has been deleted to the database.</p>
        </div>
    </div>

    <div class="row">
        <div ng-show="deleteFail" class="alert alert-danger" role="alert">
            <p>There was an error deleting the type from the database.</p>
        </div>
    </div>

    <div class="row mar-bot-15">
        <button ng-hide="showForm" class="btn btn-primary" ng-click="setupTypeForm();showForm = true;showAddButton = true; ">
            <span class="glyphicon glyphicon glyphicon-plus"></span>&nbsp;Create
        </button>
        <button ng-show="showForm" class="btn btn-danger" ng-click="clearTypeForm();showForm = false;">
            <span class="glyphicon glyphicon glyphicon-remove"></span>&nbsp;Cancel
        </button>
    </div>

    <div id="addForm" ng-show="showForm" class="row animate-show">
        <div class="row">
            <input ng-model="id"  class="form-control" type="hidden">
            <div class="col-md-2">
                <label class="control-label">Name Short</label>
                <input ng-model="name_short"  class="form-control" type="text" placeholder="IPA" maxlength="10">
            </div>
            <div class="col-md-2">
                <label class="control-label">Name Long</label>
                <input class="form-control" ng-model="name_long" placeholder="India Pale Ale" maxlength="25">
            </div>
            <div class="col-md-5">
                <!-- Set type -->
                <label class="control-label">Description</label>
                <input class="form-control" ng-model="description" maxlength="200" placeholder="Bitter, hoppy beer.">

            </div>
        </div>
        <div class="row mar-top-15" >
            <div class="col-md-4 ">
                <button ng-show="showAddButton" ng-disabled="!(!!name_short &&!!name_long && !!description)"
                        ng-click="addType()" class="btn btn-group btn-primary">
                    <span class="glyphicon glyphicon glyphicon-floppy-save"></span>&nbsp;Add
                </button>
                <button ng-show="showUpdateButton" ng-disabled="!(!!name_short &&!!name_long && !!description)"
                        ng-click="updateType()" class="btn btn-group btn-primary">
                    <span class="glyphicon glyphicon glyphicon-floppy-save"></span>&nbsp;Update
                </button>
                <button class="btn btn-danger btn-group" ng-click="clearTypeForm();">
                    <span class="glyphicon glyphicon glyphicon-repeat"></span>&nbsp;Clear
                </button>
            </div>
        </div>
    </div>

    <div class="row mar-bot-15">
        <div class="col-md-1">
            <h4>Page: {{ currentPage }}</h4>
        </div>
        <div class="col-md-2">
            <label for="search">Search:</label>
            <input ng-model="filter" id="search" class="form-control" placeholder="Filter text">
        </div>
        <div class="col-md-2">
            <label for="search">items per page:</label>
            <input type="number" min="1" max="99" class="form-control width-33" ng-model="pageSize">
        </div>
    </div>

    <div class="row mar-top-15">
        <table class="table table-striped" data-ng-init="getTypes()" >
            <tr>
                <th>Name Short</th>
                <th>Name Long</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            <tr dir-paginate="type in types | filter:filter : false| itemsPerPage: pageSize" current-page="currentPage">
                <td><a href="/type/{{type.id}}">{{type.name_short}}</td>
                <td>{{type.name_long}}</td>
                <td>{{type.description}}</td>
                <td>
                    <button class="btn btn-success" ng-click="fillTypeForm($index);">
                        <span class="glyphicon glyphicon glyphicon-edit"></span>
                    </button>
                    <button class="btn btn-danger" ng-click="deleteType(type.id)" >
                        <span class="glyphicon glyphicon glyphicon-trash"></span>
                    </button>
                </td>
            </tr>
        </table>
        <div ng-controller="OtherController" class="other-controller">
            <div class="text-center">
                <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="../dirPagination.tpl.html"></dir-pagination-controls>
            </div>
        </div>

    </div>