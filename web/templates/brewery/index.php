<?php
/**
 * Created by PhpStorm.
 * User: Davey
 * Date: 8/21/16
 * Time: 1:43 PM
 */require_once "templates/header.php";
?>

<div class="container theme-showcase" role="main"
     ng-app="tapRoom" ng-controller="beerController"
     ng-init="getBreweries();" ng-init="">
    <div class="jumbotron row mar-bot-15">
        <h1>Breweries   <p>These are the breweries currently in the system.</p></h1>
    </div>

    <!-- Error Messages -->
    <div class="row">
        <div ng-show="saveSuccess" class="alert alert-success " role="alert">
            <p>The brewery has been saved to the database.</p>
        </div>
    </div>

    <div class="row">
        <div ng-show="saveFail" class="alert alert-warning" role="alert">
            <p>There was an error saving the brewery to the database.</p>
        </div>
    </div>

    <div class="row">
        <div ng-show="deleteSuccess" class="alert alert-success" role="alert">
            <p>The brewery has been deleted to the database.</p>
        </div>
    </div>

    <div class="row">
        <div ng-show="deleteFail" class="alert alert-warning" role="alert">
            <p>There was an error deleting the brewery from the database.</p>
        </div>
    </div>


    <div class="row mar-bot-15">
        <button ng-hide="showForm" class="btn btn-primary" ng-click="setupBreweryForm();showForm = true;showAddButton = true; ">
            <span class="glyphicon glyphicon glyphicon-plus"></span>&nbsp;Create
        </button>
        <button ng-show="showForm" class="btn btn-danger" ng-click="clearBreweryForm();showForm = false;">
            <span class="glyphicon glyphicon glyphicon-remove"></span>&nbsp;Cancel
        </button>
    </div>


    <div id="addForm" ng-show="showForm" class="row animate-show">
        <div class="row">
            <input ng-model="id"  class="form-control" type="hidden">
            <div class="col-md-2">
                <label class="control-label">Name</label>
                <input ng-model="name"  class="form-control" type="text" placeholder="Alpine" maxlength="50">
            </div>
            <div class="col-md-1">
                <label class="control-label">State</label>
                <input class="form-control" ng-model="state" placeholder="CA" maxlength="2">
            </div>
            <div class="col-md-5">
                <!-- Set Brewery -->
                <label class="control-label">Description</label>
                <input class="form-control" ng-model="description" maxlength="200">

            </div>
            <div class="col-md-3">
                <label class="control-label">URL</label>
                <input class="form-control" ng-model="url" maxlength="100">
            </div>
        </div>
        <div class="row mar-top-15" >
            <div class="col-md-4 ">
                <button ng-show="showAddButton" ng-disabled="!(!!name && !!description  && !!state && !!url)"
                        ng-click="addBrewery()" class="btn btn-group btn-primary">
                    <span class="glyphicon glyphicon glyphicon-floppy-save"></span>&nbsp;Add
                </button>
                <button ng-show="showUpdateButton" ng-disabled="!(!!name && !!description  && !!state && !!url)"
                        ng-click="updateBrewery()" class="btn btn-group btn-primary">
                    <span class="glyphicon glyphicon glyphicon-floppy-save"></span>&nbsp;Update
                </button>
                <button class="btn btn-danger btn-group" ng-click="clearBreweryForm();">
                    <span class="glyphicon glyphicon glyphicon-repeat"></span>&nbsp;Clear
                </button>
            </div>
        </div>
    </div>

    <div class="row mar-bot-15">
        <div class="col-md-1">
            <h4>Page: {{ currentPage }}</h4>
        </div>
        <div class="col-md-2">
            <label for="search">Search:</label>
            <input ng-model="filter" id="search" class="form-control" placeholder="Filter text">
        </div>
        <div class="col-md-2">
            <label for="search">items per page:</label>
            <input type="number" min="1" max="99" class="form-control width-33" ng-model="pageSize">
        </div>
    </div>

    <div class="row mar-top-15">
        <table class="table table-striped" data-ng-init="getBreweries()" >
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>State</th>
                <th>URL</th>
                <th>Actions</th>
            </tr>
            <tr dir-paginate="brewery in breweries | filter:filter : false| itemsPerPage: pageSize" current-page="currentPage">
                <td><a href="/brewery/{{brewery.id}}">{{brewery.name}}</td>
                <td>{{brewery.description}}</td>
                <td>{{brewery.state}}</td>
                <td><a href="{{brewery.url}}" target="_blank">{{brewery.url}}</td>
                <td>
                    <button class="btn btn-success" ng-click="fillBreweryForm($index);">
                        <span class="glyphicon glyphicon glyphicon-edit"></span>
                    </button>
                    <button class="btn btn-danger" ng-click="deleteBrewery(brewery.id)" >
                        <span class="glyphicon glyphicon glyphicon-trash"></span>
                    </button>
                </td>
            </tr>
        </table>
        <div ng-controller="OtherController" class="other-controller">
            <div class="text-center">
                <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="../dirPagination.tpl.html"></dir-pagination-controls>
            </div>
        </div>

    </div>
</div>