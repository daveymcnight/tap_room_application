<?php
/**
 * Created by PhpStorm.
 * User: dmcnight
 * Date: 8/2/16
 * Time: 3:55 PM
 */


?>
<!DOCTYPE html>
<html>
<head>
    <title>Whats on Tap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-hQpvDQiCJaD2H465dQfA717v7lu5qHWtDbWNPvaTJ0ID5xnPUlVXnKzq7b8YUkbN" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="../app/assets/styles.css">
    <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
    <script src="/app/assets/scripts.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular-animate.js"></script>
    <script src="/app.js"></script>
    <script src="/dirPagination.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">On Tap</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">WOT?</a></li>
                <li><a href="/change-tap">Change Tap</a></li>
                <li><a href="/beer">Beers</a></li>
                <li><a href="/brewery">Breweries</a></li>
                <li><a href="/type">Types</a></li>
            </ul>
            <ul id="adminbar" class="nav navbar-nav  navbar-right">
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
