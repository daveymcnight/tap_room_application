<?php
/**
 * Created by PhpStorm.
 * User: dmcnight
 * Date: 8/2/16
 * Time: 3:58 PM
 */
require_once "templates/header.php";
?>

<div class="container " id="main" ng-app="tapRoom" ng-controller="beerController" ng-init="getBeers();">

    <!-- Heading   -->
    <?php require_once "templates/layouts/toolbar.php" ?>
    <!-- error messages   -->
    <?php require_once "templates/layouts/messages.php"?>
        
    <?php require_once "templates/beer/crud-form.php"?>
        

        <div class="row mt2">
            <table class="table table-striped">
                <tr >
                    <th>Name</th>
                    <th>Brewery</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                <tr dir-paginate="beer in beers | filter:filter : false| itemsPerPage: pageSize" current-page="currentPage">
                    <td><a href="/beer/{{beer.id}}">{{beer.beer_name}}</td>
                    <td>{{beer.brewery_name}}</td>
                    <td>{{beer.type_name_long}}</td>
                    <td>{{beer.description}}</td>
                    <td>
                        <button class="btn btn-success" ng-click="fillBeerForm($index);">
                            <span class="glyphicon glyphicon glyphicon-edit"></span>
                        </button>
                        <button class="btn btn-danger" mw-confirm-click="deleteBeer(beer.id)" mw-confirm-click-message="Are you sure you want to delete the beer {{beer.beer_name}} from the DB?" >
                            <span class="glyphicon glyphicon glyphicon-trash"></span>
                        </button>
                    </td>
                </tr>
            </table>
            <div ng-controller="OtherController" class="other-controller">
                <div class="text-center">
                    <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="../dirPagination.tpl.html"></dir-pagination-controls>
                </div>
            </div>

        </div>
</div>
