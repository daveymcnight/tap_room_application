<?php
/**
 * Created by PhpStorm.
 * User: dmcnight
 * Date: 8/3/16
 * Time: 1:41 PM
 */

require_once "templates/header.php";
?>
<div class="container theme-showcase " role="main" ng-app="tapRoom" ng-controller="tapController" data-ng-init="getTaps();getBeers();">
    <div class="col-xs-12 row mt3">
        <h1>Change Tap</h1>
        <p>This will change what beers are on tap. Careful this data is live on the site and app.</p>
    </div>
    <label class="form-control-label">At this time you can only save one are a time.</label>
    <div class="row">
        <div ng-show="saveSuccess" class="alert alert-success " role="alert">
            <p>The tap has been saved to the database.</p>
        </div>
    </div>

    <div class="row">
        <div ng-show="saveFail" class="alert alert-danger" role="alert">
            <p>There was an error saving the tap to the database.</p>
        </div>
    </div>

    <div class="row mar-top-15">
        <table class="table table-striped width-50 inline-block" >
            <tr>
                <th>Tap</th>
                <th>Beer</th>
                <th>Save</th>
                <th>Flag (Coming Soon)</th>
            </tr>
            <tr ng-repeat="tap in taps" >
                <td>{{tap.id}}</td>
                <td>
                    <select class="form-control " ng-model="tap.beer_id"
                            ng-options="beer.id as beer.beer_name for beer in beers" >
                    </select>
                </td>
                <td>
                    <button class="btn btn-group btn-primary" ng-click="updateTap($index)">Save</button>
                </td>
                <td>{{tap.tag_id}}N/A</td>
            </tr>
        </table>
    </div>