<?php
/**
 * Created by PhpStorm.
 * User: dmcnight
 * Date: 8/3/16
 * Time: 9:49 AM
 */

?>

<div id="addForm" ng-show="showForm" class="row animate-show " >
    <div class="row mt1">
        <input ng-model="id"  class="form-control" type="hidden">
        <div class="col-md-3">
            <label class="control-label">Beer Name</label>
            <input ng-model="beer_name"  class="form-control" type="text" placeholder="Duet" maxlength="50">
        </div>
        <div class="col-md-5">
            <label class="control-label">Description</label>
            <input class="form-control" ng-model="description" placeholder="Beer Description that will show up on both apps." maxlength="300">
        </div>
        <div class="col-md-2" ng-init="getBreweries();">
            <!-- Set Brewery -->
            <label class="control-label">Brewery</label>
            <select class="form-control" ng-model="brewery_id"
                    ng-options="brewery.id as brewery.name for brewery in breweries" >
            </select>
        </div>
        <div class="col-md-1" ng-init="getTypes();">
            <label class="control-label">Type</label>
            <select class="form-control" ng-model="type_id"
                    ng-options="type.id as type.name_short for type in types">
            </select>
        </div>

        <div class="col-md-1">
            <label class="control-label">Abv</label>
            <input ng-model="abv" class="form-control" type="text" placeholder="6.2"
                   onkeypress='validateNumber(event)' maxlength="5">
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
            <label class="control-label">$ Pint</label>
            <input ng-model="price_pint" class="form-control" type="text"
                   onkeypress='validateNumber(event)' maxlength="5">
        </div>

        <!-- Set Beer Price 32oz  -->
        <div class="col-md-1">
            <label class="control-label">$ 32oz</label>
            <input ng-model="price_32_oz" class="form-control" type="text"
                   onkeypress='validateNumber(event)' maxlength="5">
        </div>

        <!-- Set Beer Price 64oz  -->
        <div class="col-md-1">
            <label class="control-label">$ 64oz</label>
            <input ng-model="price_64_oz" class="form-control" type="text"
                   onkeypress='validateNumber(event)' maxlength="5">
        </div>
        <!-- Set Beer Price 2 Liter  -->
        <div class="col-md-1">
            <label class="control-label">$ 2L</label>
            <input ng-model="price_2liter" class="form-control" type="text"
                   onkeypress='validateNumber(event)' maxlength="5">
        </div>

        <div class="col-md-1">
            <label class="control-label">10oz?</label>
            <select class="form-control" ng-model="is_10_oz" >
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
        </div>
        <div class="col-md-1">
            <label class="control-label">Growler?</label>
            <select class="form-control" ng-model="is_growler_available">
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
        </div>

        <div class="col-md-5">
            <label class="control-label">Beer Advocate Url</label>
            <input ng-model="url" class="form-control" type="text"
                   placeholder="http://www.beeradvocate.com/beer/profile/3120/27604" maxlength="100">
        </div>

        <div class="col-md-1">
            <label class="control-label">Rating</label>
            <input ng-model="rating" class="form-control form-control-small" type="text" placeholder="96"
                   onkeypress='validateNumber(event)' maxlength="2">
        </div>
    </div>
    <div class="row mt1" >
        <div class="col-md-4 ">
            <button ng-show="showAddButton" ng-disabled="!(!!beer_name && !!description  && !!brewery_id  && !!type_id && !!abv &&
                                   !!price_pint  && !!price_32_oz  && !!price_64_oz  && !!price_2liter &&
                                   !!url && !!rating && !!is_growler_available && !!is_10_oz)"
                    ng-click="addBeer()" class="btn btn-group btn-primary">
                <span class="glyphicon glyphicon glyphicon-floppy-save"></span>&nbsp;Add
            </button>
            <button ng-show="showUpdateButton" ng-disabled="!(!!beer_name && !!description  && !!brewery_id  && !!type_id && !!abv &&
                                   !!price_pint  && !!price_32_oz  && !!price_64_oz  && !!price_2liter &&
                                   !!url && !!rating && !!is_growler_available && !!is_10_oz)"
                    ng-click="updateBeer()" class="btn btn-group btn-primary">
                <span class="glyphicon glyphicon glyphicon-floppy-save"></span>&nbsp;Update
            </button>
            <button class="btn btn-danger btn-group" ng-click="clearBeerForm();">
                <span class="glyphicon glyphicon glyphicon-repeat"></span>&nbsp;Clear
            </button>
        </div>
    </div>
</div>
