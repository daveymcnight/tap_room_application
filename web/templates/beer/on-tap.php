<?php
/**
 * Created by PhpStorm.
 * User: dmcnight
 * Date: 8/3/16
 * Time: 1:31 PM
 */

require_once "templates/header.php";
?>

<div class="container theme-showcase"
     role="main" ng-app="tapRoom"
     ng-controller="beerController" ng-init="getBeersOnTap();">
    <div class="col-xs-12 mln1 mt3">
        <h1 class="">Beers on Tap</h1>
    </div>


    <table class="table table-striped">
        <tr>
            <th>Tap</th>
            <th>Name</th>
            <th>Brewery</th>
            <th>State</th>
            <th>Type</th>
            <th>Description</th>
            <th>Pint</th>
            <th>32oz/64oz/2Liter</th>
        </tr>
        <tr ng-repeat="beer in beersOnTap">
            <td>{{beer.id}}</td>
            <td>{{beer.beer_name}}</td>
            <td>{{beer.brewery_name}}</td>
            <td>{{beer.brewery_state}}</td>
            <td>{{beer.beer_type_name}}</td>
            <td>{{beer.description}}</td>
            <td>{{beer.price_pint}}</td>
            <td>{{beer.price_32_oz}}/{{beer.price_64_oz}}/{{beer.price_2liter}}</td>
        </tr>
    </table>
</div>
