<?php
/**
 * Created by PhpStorm.
 * User: dmcnight
 * Date: 8/3/16
 * Time: 9:31 AM
 */ 
?>
        <!-- Error Messages -->
        <div class="row">
            <div ng-show="saveSuccess" class="alert alert-success " role="alert">
                <p>The Beer has been saved to the database.</p>
            </div>
        </div>

        <div class="row">
            <div ng-show="saveFail" class="alert alert-danger" role="alert">
                <p>There was an error saving the beer to the database.</p>
            </div>
        </div>

        <div class="row">
            <div ng-show="deleteSuccess" class="alert alert-success" role="alert">
                <p>The Beer has been deleted to the database.</p>
            </div>
        </div>

        <div class="row">
            <div ng-show="deleteFail" class="alert alert-danger" role="alert">
                <p>There was an error deleting the beer from the database.</p>
            </div>
        </div>