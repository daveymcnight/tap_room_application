<?php
/**
 * Created by PhpStorm.
 * User: dmcnight
 * Date: 8/3/16
 * Time: 9:33 AM
 */
?>

<div class="row mt3 " role="main">
    <div class=" col-xs-12 mt2  pl0">
        <div class="col-xs-2 mln1">
            <h1 class="">Beers</h1>
        </div>
        <div class="mt1 col-xs-10">
            <div class="col-md-2 mt1">
            <button ng-hide="showForm" class="btn btn-primary" ng-click="setupAddBeer();showForm = true;showAddButton = true; ">
                <span class="glyphicon glyphicon glyphicon-plus"></span>&nbsp;Create
            </button>
            <button ng-show="showForm" class="btn btn-danger" ng-click="clearBeerForm();showForm = false;">
                <span class="glyphicon glyphicon glyphicon-remove"></span>&nbsp;Cancel
            </button>
        </div>

            <div class="col-md-4">
                <label for="search">Search:</label>
                <input ng-model="filter" id="search" class="form-control" placeholder="Filter text">
            </div>
            <div class="col-md-2">
                <label for="search">items per page:</label>
                <input type="number" min="1" max="99" class="form-control width-33" ng-model="pageSize">
            </div>
            <div class="col-md-2">
                <h4>Page: {{ currentPage }}</h4>
            </div>
        </div>
    </div>
</div>