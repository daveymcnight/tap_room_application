angular.module('starter.controllers', [])

.controller('OnTapController', ['$scope','$http', function($scope, $http) {

  var api = "http://davidmcnight.com/api/";

  $scope.getBeersOnTap = function () {
    $http.get(api+"Beer/onTap").success(function (response) {
      $scope.beersOnTap = response;
    });
    // $http.get("/api/Beer/onTap").success(function (response) {
    //     $scope.beersOnTap = response;
    //   });
  }
}]).controller('BeerController',['$scope','$http','$stateParams', function($scope, $http, $stateParams) {

    var api = "http://davidmcnight.com/api/";
    console.log($stateParams.beerId);

    $scope.getBeer = function () {
      $http.get(api + "Beer/" + $stateParams.beerId).success(function (response) {
        $scope.beer = response;
      });
    }
}])



.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});


// .controller('ChatsCtrl', function($scope, Chats) {
//   // With the new view caching in Ionic, Controllers are only called
//   // when they are recreated or on app start, instead of every page change.
//   // To listen for when this page is active (for example, to refresh data),
//   // listen for the $ionicView.enter event:
//   //
//   //$scope.$on('$ionicView.enter', function(e) {
//   //});
//
//   $scope.chats = Chats.all();
//   $scope.remove = function(chat) {
//     Chats.remove(chat);
//   };
// })
